import mongoose, { Connection } from "mongoose";
import { loggers } from "winston";

const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_URL = process.env.DB_URL;

const logger = loggers.get("pantryLogger");

// Create new pooled MongooseClient
mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });
const pooledMongooseClient: Connection = mongoose.connection;

pooledMongooseClient.on('connecting', event => console.log("Mongoose: pooled connecting"));
pooledMongooseClient.on('connected', event => console.log("Mongoose: pooled connected"));
pooledMongooseClient.on('open', event => console.log("Mongoose: pooled open"));
pooledMongooseClient.on('disconnecting', event => console.log("Mongoose: pooled disconnecting"));
pooledMongooseClient.on('disconnected', event => console.log("Mongoose: pooled disconnected"));
pooledMongooseClient.on('close', event => console.log("Mongoose: pooled close"));
pooledMongooseClient.on('reconnected', event => console.log("Mongoose: pooled reconnected"));
pooledMongooseClient.on('fulllsetup', event => console.log("Mongoose: pooled fulllsetup"));
pooledMongooseClient.on('error', event => console.log("Mongoose: pooled error"));
pooledMongooseClient.on('all', event => console.log("Mongoose: pooled all"));

export { mongoose }