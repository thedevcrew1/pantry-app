import {mongoose} from "../init";

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
	value: String
}, {collection: 'category'});

const Category = mongoose.model("Category", CategorySchema);

export {Category}