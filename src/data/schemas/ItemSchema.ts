import {mongoose} from "../init";

const Schema = mongoose.Schema;

const ItemSchema = new Schema({
    product: {
        // @ts-ignore
        type: mongoose.Schema.ObjectId,
        ref: "product"
    },
    purchasedDate: String,
    expirationDate: String,
    pantry: {
        // @ts-ignore
        type: mongoose.Schema.ObjectId,
        ref: "Pantry"
    },
    state: {
        // @ts-ignore
        type: mongoose.Schema.ObjectId,
        ref: "ItemState"
    },
    shoppingList: {
        // @ts-ignore
        type: mongoose.Schema.ObjectId,
        ref: "ShoppingList"
    }
}, {collection: 'item'});

const Item = mongoose.model('Item', ItemSchema);

export { Item }