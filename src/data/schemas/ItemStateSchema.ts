import {mongoose} from '../init';

const Schema = mongoose.Schema;

const ItemStateSchema = new Schema({
    prev: Array,
    next: Array,
    value: String
});

const ItemState = mongoose.model('ItemState', ItemStateSchema);

export { ItemState }