import {mongoose} from "../init";

const Schema = mongoose.Schema;

const PantryOwnershipSchema = new Schema({
    user: {
        // @ts-ignore
        type: mongoose.Schema.ObjectId,
        ref: "User"
    },
    pantry: {
        // @ts-ignore
        type: mongoose.Schema.ObjectId,
        ref: "Pantry"
    }
}, {collection: 'pantryOwnership'});

const PantryOwnership = mongoose.model('PantryOwnership', PantryOwnershipSchema);

export { PantryOwnership }