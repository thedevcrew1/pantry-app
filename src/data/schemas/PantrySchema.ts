import {mongoose} from "../init";

const Schema = mongoose.Schema;

const PantrySchema = new Schema({}, {collection: 'pantry'});

const Pantry = mongoose.model('Pantry', PantrySchema);

export { Pantry }