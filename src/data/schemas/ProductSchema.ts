import {mongoose} from "../init";

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	name: String,
	category: {
		// @ts-ignore
		type: mongoose.Schema.ObjectId,
		ref: "Category"
	},
	expirationPeriod: String // Interval in a format recognized by Swift.
}, {collection: 'product'});

const Product = mongoose.model("Product", ProductSchema);

export {Product}