import bcrypt from "bcrypt";

import {mongoose} from "../init";
import {NextFunction, Request, Response} from "express";


const Schema = mongoose.Schema;

const saltRounds: number = 10;

// Definition
const UserSchema = new Schema({
    username: String,
    firstName: String,
    lastName: String,
    password: String,
    email: String,
}, { collection: 'user' });

// Middleware methods
UserSchema.pre("save", async function (next: NextFunction) {
    if (!this.isModified("password")) {
        return next();
    }

    const hash: string = await bcrypt.hash(this.get("password"), saltRounds);
    this.set("password", hash);
    next();
});

// Class methods
UserSchema.static('checkForUserUsername', async (username: string): Promise<boolean> => {
    return await mongoose.connection.collection("user").countDocuments({ username }, { limit: 1 }) === 1;
});

// Object methods
UserSchema.method("comparePassword", async function (password: string): Promise<boolean> {
    const hash: string = this.password;

    try {
        return bcrypt.compare(password, hash)

    } catch (e) {
        console.log(e); // TODO log error.
        return false;
    }
});

UserSchema.method("toJSON", function (): object {

    // Remove password from response so we never send the password across the wire.
    const object = this.toObject();
    delete object.password;

    // Format _id as id for the client.
    object.id = object._id;
    delete object._id;

    return object;
});

// Middleware function to check for logged-in users using the session.
const authMW = async (req: Request, res: Response, next: NextFunction) => {
    // @ts-ignore
    if (!req.session || !req.session.userID)
        return res.sendStatus(401);

    try {

        const user = await User.findOne({
            // @ts-ignore
            _id: req.session.userID
        });

        // If the user doesn't exist a false cookie was provided.
        if (!user) {
            return res.sendStatus(401);
        }

        // Set the user field in the request. This means we can access the user from any other endpoint that has this
        // middleware enabled on it.
        // @ts-ignore
        req.user = user;

    } catch (error) {
        // Return an error if user does not exist.
        return res.sendStatus(401);
    }

    // if everything succeeds, move to the next middleware
    next();
};

const User = mongoose.model('User', UserSchema);

export {
    User,
    authMW
}