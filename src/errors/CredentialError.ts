class CredentialError extends Error {
    constructor(message: string) {
        super();
        this.message = message;
    }
}

export { CredentialError }