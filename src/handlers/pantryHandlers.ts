import { NextFunction, Request, Response } from "express";
import { Pantry } from '../data/schemas/PantrySchema';
import { PantryOwnership } from '../data/schemas/PantryOwnership';
import { Item } from '../data/schemas/ItemSchema';
import * as _ from 'lodash'

const getPantry = async (req: Request, res: Response) => {

    try {
        // @ts-ignore
        const user = req.user;

        const pantries = await PantryOwnership.find({ user })
            .populate('pantry');

        // The reason for first creating a json object is it acts as a hash map, where the lookup is O(1). This keeps the
        // overall complexity of the function at O(n) where n is the number of items in the pantry.
        const result = {};
        for(const pantry of pantries) {
            const items = await Item.find({
                pantry
            }).populate('product');

            // @ts-ignore
            for(const item of items) {
                // @ts-ignore
                if (!result.hasOwnProperty(item.product.id)) {
                    // @ts-ignore
                    const productCopy = _.cloneDeep(item.product);
                    productCopy.items = [];
                    // @ts-ignore
                    result[item.product.id] = productCopy;
                }

                // @ts-ignore
                result[item.product.id].items.push(item);
            }
        }

        const formattedResults = [];
        const resultKeys = Object.keys(result);

        for (const key of resultKeys) {
            // @ts-ignore
            formattedResults.push(result[key]);
        }

        return res.send(formattedResults);

    } catch (e) {
        console.log(e);
        return res.sendStatus(500)
    }
};


export { getPantry }