import { NextFunction, Request, Response } from "express";

import { LoginService } from "../services/LoginService"
import { RegisterService } from "../services/RegisterService";
import { ValidationError } from "../errors/ValidationError";
import { CredentialError } from "../errors/CredentialError";
import { UpdateUserService } from "../services/UpdateUserService";

const registerHandler = async (req: Request, res: Response, next: NextFunction) => {
    const registerService: RegisterService = new RegisterService();

    try {
        const registeredUser = await registerService.registerUser(req.body);
        res.status(200).send({
            user: registeredUser
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            res.status(400).send({
                message: error.message
            });

        } else if (error instanceof CredentialError) {
            res.status(400).send({
                message: error.message
            });

        } else {
            next(error.message);
        }
    }
};

const loginHandler = async (req: Request, res: Response, next: NextFunction) => {
    const loginService: LoginService = new LoginService();

    try {
        const loggedInUser: object = await loginService.executeLogin(req);
        res.status(200).send({
            user: loggedInUser
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            res.status(400).send(error.message);
        } else if (error instanceof CredentialError) {
            res.status(401).send(error.message);
        } else {
            next(error.message);
        }
    }
};

const updateUserHandler = async (req: Request, res: Response) => {
    const service = new UpdateUserService();
    await service.execute(req, res);
};



export {
    registerHandler,
    loginHandler,
    updateUserHandler,
}