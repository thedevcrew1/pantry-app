import express, { Router } from "express"
import * as pantryHandlers from "../handlers/pantryHandlers"
import {authMW} from "../data/schemas/UserSchema";

const pantryRouter: Router = express.Router();

pantryRouter.get('/', authMW, pantryHandlers.getPantry);

export { pantryRouter }