import express, { Router } from "express"
import * as userHandlers from "../handlers/userHandlers"
import { authMW } from "../data/schemas/UserSchema";

const userRouter: Router = express.Router();

userRouter.post("/", userHandlers.registerHandler);
userRouter.post("/login", userHandlers.loginHandler);
userRouter.put("/", authMW, userHandlers.updateUserHandler);

export { userRouter };