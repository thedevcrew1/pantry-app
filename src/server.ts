import dotenv from "dotenv";
dotenv.config();

import winston, { loggers } from "winston";

winston.loggers.add("pantryLogger", {
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.label({ label: "pantryLogger" }),
        winston.format.json()
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ 
            filename: 'logs/combined.log'
        })
    ]
});

import bodyParser from "body-parser"
import cookieParser from "cookie-parser";
import cookieSession from 'cookie-session'
import express, { Express, NextFunction, Response, Request } from "express"
import morgan from "morgan";

import { mwRouter } from "./middleware"
import { userRouter } from "./routes/userRouter";
import { pantryRouter } from "./routes/pantryRoutes"

const app: Express = express();

let port: string;
if (process.env.PORT) {
    port = process.env.PORT;
} else {
    // default port to listen on
    port = "8080";
}

// http logger
app.use(morgan('tiny'));

// enable cookie parser
app.use(cookieParser());

// This sets us up to use cookie sessions
app.use(cookieSession({
    name: 'session',
    keys: ['secretValue'],
    // @ts-ignore
    cookie: {
        maxAge: 60 * 60 * 1000 // 1 Hour expiration.
    }
}));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// mount to the middleware router
app.use(mwRouter);

// default route
app.get("/", (req: Request, res: Response) => {
    res.send("Default page.");
});

// mount routers
app.use("/user", userRouter);
app.use("/pantry", pantryRouter);

// custom catch-all error handler
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).send(err);
});

const server = app.listen( port, () => {
    console.log( `Server listening on http://localhost:${ port } ...` );
});

// watch server crash/close server connection
server.on("close", () => {
    console.log('Doh :(');
    console.log('Closing DB connection');
    // mongoClient.close();
});