import { Document } from "mongoose";
import { Pantry } from "../data/schemas/PantrySchema";
import { User } from "../data/schemas/UserSchema";
import { PantryOwnership } from "../data/schemas/PantryOwnership";
import {ValidationError} from "../errors/ValidationError";


class CreatePantryService {
    async execute(ownerUsername: string) {
        const user = await User.find({
            username: ownerUsername
        });

        if(!user) {
            throw new ValidationError("A valid user is required.");
        }

        // @ts-ignore
        this.createPantryAndOwnership(user.id);
    }

    async executeWithOwnerID(ownerID: string) {
        this.createPantryAndOwnership(ownerID);
    }

    private async createPantryAndOwnership(userID: string) {
        const pantry = new Pantry({
        });

        await pantry.save();

        const pantryOwnership = new PantryOwnership({
            user: userID,
            pantry: pantry.id
        });

        await pantryOwnership.save();
    }
}

export { CreatePantryService }