import bcrypt from "bcrypt";
import validator from "validator";

import { Document } from "mongoose";

import { ValidationError } from "../errors/ValidationError";
import { CredentialError } from "../errors/CredentialError";
import { Request } from "express"
import { User } from "../data/schemas/UserSchema";

class LoginService {
    validate(reqBody: any): boolean {
        if (!("password" in reqBody)) {
            return false;
        } else if (!("username" in reqBody)) {
            return false;
        }

        if (!validator.isAscii(reqBody.password)) {
            return false;
        } else if (!validator.isAlphanumeric(reqBody.username)) {
            return false;
        }

        return true;
    }

    async executeLogin(req: Request): Promise<object> {

        const reqBody = req.body;
        const validReqBody: boolean = this.validate(reqBody);

        if (!validReqBody) {
            throw new ValidationError("Invalid login input(s)");
        }

        // @ts-ignore
        const username: string = reqBody.username;

        const user: Document = await User.findOne({ username });

        if(!user) {
            throw new CredentialError("Username does not exist");
        }

        // @ts-ignore
        const password: string = reqBody.password;

        // @ts-ignore: Mongoose doesn't play nice with TS
        const correctPassword: boolean = await user.comparePassword(password);

        if (!correctPassword) {
            throw new CredentialError("Invalid credentials");
        }

        // Set the userID in the session so we can retrieve it later for session resumption.
        // @ts-ignore
        req.session.userID = user.id;

        return user
    }
}

export { LoginService }