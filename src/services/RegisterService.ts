import validator from "validator";

import { Document } from "mongoose";

import { ValidationError } from "../errors/ValidationError";
import { CredentialError } from "../errors/CredentialError";
import { User } from "../data/schemas/UserSchema";
import { CreatePantryService } from './CreatePantryService'

class RegisterService {
    validate(reqBody: any): boolean {
        if (!("username" in reqBody)) {
            return false;
        } else if (!("firstName" in reqBody)) {
            return false;
        } else if (!("lastName" in reqBody)) {
            return false;
        } else if (!("password" in reqBody)) {
            return false;
        } else if (!("email" in reqBody)) {
            return false;
        }

        if (!validator.isAlphanumeric(reqBody.username)) {
            return false;
        } else if (!validator.isAlpha(reqBody.firstName)) {
            return false;
        } else if (!validator.isAlpha(reqBody.lastName)) {
            return false;
        } else if (!validator.isAscii(reqBody.password)) {
            return false;
        } else if (!validator.isEmail(reqBody.email)) {
            return false;
        }

        return true;
    }

    async registerUser(reqBody: any): Promise<Document> {
        const validReqBody: boolean = this.validate(reqBody);

        if (!validReqBody) {
            throw new ValidationError("Invalid register input(s)");
        }

        const username: string = reqBody.username;
        const password: string = reqBody.password;

        // @ts-ignore: Mongoose not play nice with TS
        const userExists: boolean = await User.checkForUserUsername(username);

        if (userExists) {
            throw new CredentialError("Username already exists");
        }

        const firstName: string = reqBody.firstName;
        const lastName: string = reqBody.lastName;
        const email: string = reqBody.email;
        const pantries: string[] = [];

        const newUser: Document = new User({ username, firstName, lastName, password, email, pantries });

        await newUser.save();

        const createPantryService = new CreatePantryService();

        await createPantryService.executeWithOwnerID(newUser.id);

        return newUser
    }
}

export { RegisterService }