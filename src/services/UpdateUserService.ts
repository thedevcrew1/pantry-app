// @ts-ignore
import {Request, Response} from '@types/express';
import {User} from "../data/schemas/UserSchema";

class UpdateUserService {

    async execute(req: Request, res: Response) {
        const updates = req.body.updates;

        if (!updates) {
            return res.status(400)
                .send({
                    message: "Updates required."
                });
        }

        const updateKeys = Object.keys(updates);
        // @ts-ignore
        const user = req.user;


        for(const key of updateKeys) {
            if(key === 'username') {
                // @ts-ignore
                const usernameInUse = await User.checkForUserUsername(updates[key]);

                if(usernameInUse) {
                    return res.status(400)
                        .send({
                            'message': "Username already in use"
                        })
                }
            }

            user[key] = updates[key];
        }

        await user.save();
        return res.sendStatus(200);
    }
}

export { UpdateUserService }