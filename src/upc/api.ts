import https from "https";
import { loggers } from "winston";

const logger = loggers.get("pantryLogger");

const opts = {
    hostname: "api.upcitemdb.com",
    path: "/prod/trial/lookup"
}

const UPCget = async (upcItem: string) => {
    opts.path = opts.path + `?upc=${upcItem}`

    logger.info(`Send GET request for data about: ${upcItem}`);
    return https.get(opts);
}

export {
    UPCget
}