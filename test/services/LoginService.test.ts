import {LoginService} from "../../src/services/LoginService";
import {expect} from "chai";

describe("LoginService tests", () => {
	describe("Validate method tests", () => {
		it("Dummy test should pass", () => {
			expect(true).to.equal(true);
		});
		it("Validate doesn't return null", () => {
			const loginService: LoginService = new LoginService();
			const req = {
				username: "Logan",
				password: "testing"
			};
			const result = loginService.validate(req);
			expect(result).to.not.equal(null);
		});
	});
});